<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'russetdesign');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?Ru9v6-l+VGH^fvljSev[B#(}v N1PY5.,)v|#[zq99FblKo&#0Da2^`h/s<(oP-');
define('SECURE_AUTH_KEY',  ':Nj9O*)2=1/Bm36YX#86qxThIn&B<XUei9Qn${K$ZkoiJv?HIR3ZD?9pz<7.Cz1p');
define('LOGGED_IN_KEY',    'YE7|6#d31A!#-((?J$a`fwlr_I7:BL-&%.H$`sA+sgm/-yUhRy`---sX-w*Uo|rP');
define('NONCE_KEY',        'oVJf<j+Cu[0HpB&-FEft99t`Oh48|_-;{jZfz zNvh0euZfp7I/Y7!|&`xw951%T');
define('AUTH_SALT',        '^]FM+N/e0cY+YdrR{Gr|#u,<C~vAQn.kh-i4E|cwlSz#r~~nt+Ws!DG`0i-~mIsl');
define('SECURE_AUTH_SALT', 'j$R!Zr{cqumLLQr{Onh*F^nD~c^,<_8llMX!|E9+vjo#v5J;!}bOjV+DPw1Yo!p5');
define('LOGGED_IN_SALT',   ']B+|g;_t+t@}j-CU;%1U.H3Q-6H.4%ig+y@44GVEog{=ksCg-vs|pYhnGkf_k4sL');
define('NONCE_SALT',       '|LcI-K C$+>RSBff`|S.|*t+(H}8U{b?42Qk,q(j75M+4m4#$`|on{=GLa|j9-j|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
