		<footer id="footer">
			<div class="wrapper">
				<ul class="links">
					<li>Art Prints | Geometric Prints | E-Decorating | Online Interoir Decoration</li>
				</ul>
				<ul class="copyright">
					<li>Copyright &copy; 2014 Russet Design | <a href="/terms">Terms</a> | <a href="/privacy">Privacy Policy</a> | Photography by Main Frame Photography</li>
				</ul>
			</div>
		</footer>
	</body>
</html>