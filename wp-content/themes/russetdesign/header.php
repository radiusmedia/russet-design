<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script src="<?php echo THEME_SCRIPTS; ?>/modernizr.js"></script>
		<link rel="stylesheet" href="<?php echo THEME_STYLES; ?>/main.css" type="text/css">
	</head>
	<body>
		<?php get_template_part('includes/header'); ?>
		<?php get_template_part('includes/header-sticky'); ?>