<?php
	define( 'THEME_URI', get_stylesheet_directory_uri() );
	define( 'THEME_IMAGES', THEME_URI . '/assets/images' );
	define( 'THEME_STYLES', THEME_URI . '/assets/css' );
	define( 'THEME_SCRIPTS', THEME_URI . '/assets/scripts' );
	add_theme_support( 'post-thumbnails', array( 'page' ) );
?>