<?php get_header(); ?>
	<div id="sliders-container"></div>
	<div id="main">
		<div class="wrapper">
			<article class="col">
				<div class="heading">
					<i class=""></i>
					<h2>Art Prints</h2>
				</div>
				<div class="col-content">
					<p>Russet Design creates beautiful, premium art prints. All prints are designed and professionaly printed on superior paper stock in Melbourne, Australia.</p>
					<p>Click here to see the collection.</p>
				</div>
			</article>
			<article class="col">
				<div class="heading">
					<i class=""></i>
					<h2>Social Media</h2>
				</div>
				<div class="col-content">
					<p>Visit our Facebook page to keep up to date with Russet news and up coming sales.</p>
					<p>Follow our boards on Pinterest to see our art inspiration and the places and spaces we love.</p>
				</div>
			</article>
		</div>
	</div>
<?php get_footer(); ?>