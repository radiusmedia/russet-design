<header id="header">
	<div class="wrapper">
		<div class="logo">
			<a href="/">
				<img src="<?php echo THEME_IMAGES; ?>/logo.png" class="normal_logo">
				<img src="<?php echo THEME_IMAGES; ?>/logo2x.png" class="retina_logo">
			</a>
		</div>
		<nav id="nav">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/prints">Prints</a></li>
				<li><a href="/framing">Framing</a></li>
				<li><a href="/buy">Buy</a></li>
				<li><a href="/interiors">Interiors</a></li>
				<li><a href="/about">About</a></li>
				<li><a href="/contact">Contact</a></li>
			</ul>
		</nav>
	</div>
</header>